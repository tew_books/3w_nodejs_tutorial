
---

TeamEverywhere Nodejs Guide
*Updated at 2019-12-01*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [TeamEverywhere](https://www.team-everywhere.com/)*


- [라우팅](#라우팅)
- [JSON](#JSON)
- [query_and_body](#query_and_body)
- [SCHOOL_POST_EDIT](#SCHOOL_POST_EDIT)
- [SCHOOL_PUT_EDIT](#SCHOOL_PUT_EDIT)
- [SCHOOL_DELETE_EDIT](#SCHOOL_DELETE_EDIT)
- [STUDENT_RESTAPI](#STUDENT_RESTAPI)
- [STUDENT_REGISTER](#STUDENT_REGISTER)
- [STUDENT_SIGNIN](#STUDENT_SIGNIN)
- [SCHOOL_DELETE_EDIT](#SCHOOL_DELETE_EDIT)
- [전체코드](#전체코드)


<!-- END doctoc generated TOC please keep comment here to allow auto update -->
## 라우팅
    - 이전에 우리는 경로에 따라 다른 핸들링을 취하는 것을 라우팅이라고 배웠습니다.
    - 과제 에서는 school과 student를 처리해야 하기 때문에, 라우팅을 통해 api 서버를 구분하기로 해봅시다.
    - student는 localhost:3000/student
      school은 localhost:3000/shcool로 호출 하도록 처리할 것입니다.
  
```javascript
    if(url.pathname == '/school'){
        // school api
    } else if(url.pathname.includes('/student')){
        // student api
    }
```


## query_and_body
    - 일반적으로 서버에 데이터를 전달하는 두가지 방법이 있습니다.
    - query: url에 요청하는 데이터를 보내는 방식입니다.
        - https://nodejs.org/api/url.html#url_urlobject_query
```javascript
   localhost:3000?user=dahan
```
    - body : post 메소드로 json 데이터를 전송할때 사용하는 프로토콜의 부분입니다.
    - 일반적으로 query는 url 경로에 비밀번호 등이 노출 되므로, 로그인 시에는 post를 사용하는 게 대부분 입니다.      
  
```javascript
    if(url.pathname == '/school'){
        // school api
    } else if(url.pathname.includes('/student')){
        // student api
    }
```


## JSON
    - 이전에는 데이터를 다음과 같이 저장했습니다.
```javascript
    [{"name":"schooltwso","address":"gangnam2","type":"middle"}]
```
    - 그런데 학생을 저장하려고 하니 새로운 걱정거리가 생깁니다.
    - 학교 데이터와 학생 데이터를 어떻게 구분할 것인가?
    - 물론 배열을 아래와 같이 나열 하는 방식이 있을 수 있습니다.
```javascript
    [ 
        //school
        [{"name":"schooltwso","address":"gangnam2","type":"middle"}],
        //student
        [{"name":"dahan","id":"test","pwd":"1234","school":"schooltwso"}]
    ]
```
    - [[학교 배열],[학생 배열]] 의 이중 배열로 저장하려고 하니, 각 데이터를 가져오려면 index로 불러와야 합니다.
    - 만약 배열이 많아지면, 관리하기 어려울 것 같은 고민이 생깁니다.
    - [[학교 배열],[학생 배열],[부모님 배열]...[선생님 배열]]
    - 그래서 아래와 같은 JSON 형태로 저장하기로 마음을 먹습니다.
```javascript
    {"scool":
        [
            {"name":"schooltwso","address":"gangnam2","type":"middle"}
        ],
    "student":
        [
            {"name":"dahan","id":"test","pwd":"1234","school":"schooltwso"}
        ]
    }
```
    - 이제 각 데이터에 이름을 지어주었습니다. 앞으로 얼마나 배열이 추가 되든 잘 관리할 수 있을 것 같습니다.


## SCHOOL_POST_EDIT
    - DB 구조가 변경 된 만큼 기존 소스도 변경이 필요 합니다.
    - 우선 School의 Post를 변경해 보겠습니다.
    - School의 REST API는 모두 url.pathname == '/school' 아래에 위치 합니다.
    - 학교와 학생 데이터에 키(이름)이 주어졌으니, Object의 점표기법으로 관련 데이터를 불러온 후 작업해야 합니다.
```javascript
    if(req.method == 'POST'){
        let body = '';
        req.on('data', function (data) {
            body += data;            
        });
        req.on('end', function () {
            const json =JSON.parse(body);
            const school = new School(json.name, json.address, json.type);
            let schoolArrays = [school];
            let newDb;
            fs.readFile('database.json', 'utf8', function(err, data) {                
                if(data){
                    let originDb = JSON.parse(data);
                    console.log('originDb : ',originDb)
                    //점 표기법으로 school을 불러옵니다.
                    let originSchoolObj = originDb.school
                    let arrayDb = Array.from(originSchoolObj);
                    arrayDb.push(school);
                    originDb.school = arrayDb;            
                    newDb = JSON.stringify(originDb)                                                             
                    //기존 데이터베이스의 school에 덮어씌워야 다른 데이터가 사라지지 않습니다.
                } else {
                    // no data exists
                    newDb = JSON.stringify({school: schoolArrays});
                }
                
                fs.writeFile('database.json', newDb, 'utf8', function(err) {
                    res.statusCode = 201;
                    res.end(JSON.stringify({result : true, list : newDb}))
                });
            });            
        });
    }
```


## SCHOOL_PUT_EDIT
    - PUT 기능 또한 변경 되었습니다.
    - Object의 점표기법으로 학교 데이터를 불러온 후 수정하도록 하겠습니다.    
```javascript
    } else if(req.method == 'PUT'){
        let body = '';
        req.on('data', function (data) {
            body += data;            
        });
        req.on('end', function () {
            const json =JSON.parse(body);
            const school = new School(json.name, json.address, json.type);
            fs.readFile('database.json', 'utf8', function(err, data) {                
                let originDb = JSON.parse(data);   
                let originSchoolObj = originDb.school                 
                let arrayDb = Array.from(originSchoolObj);
                arrayDb[json.index]  = school;
                originDb.school = arrayDb;            
                let newDb = JSON.stringify(originDb)             
                fs.writeFile('database.json', newDb, 'utf8', function(err) {
                    res.statusCode = 201;
                    res.end(JSON.stringify({result : true, list : arrayDb}))
                });
            });
        });   
    }
```


## SCHOOL_DELETE_EDIT
```javascript
    } else if(req.method == 'DELETE'){
        let body = '';
        req.on('data', function (data) {
            body += data;            
        });
        req.on('end', function () {
            const json =JSON.parse(body);
            fs.readFile('database.json', 'utf8', function(err, data) {                
                let originDb = JSON.parse(data);                    
                let originSchoolObj = originDb.school                 
                let arrayDb = Array.from(originSchoolObj);
                delete arrayDb[json.index] 
                arrayDb = arrayDb.filter(x => x != null)   
                originDb.school = arrayDb;            
                let newDb = JSON.stringify(originDb)
                fs.writeFile('database.json', newDb, 'utf8', function(err) {
                    res.statusCode = 201;
                    res.end(JSON.stringify({result : true, list : arrayDb}))
                });
            });
        }); 
    }
```


## STUDENT_RESTAPI
    - 학생의 REST API는 url.pathname.includes('/student') 아래에 포함되도록 했습니다.
    - Student REST API의 POST는 로그인과 회원가입을 진행해야 하고, 이에 대한 라우팅 주소가 달라야 하기 때문입니다.
    - 저는 아래와 같이 라우팅 주소를 설정했습니다.
        - /student : 회원가입
        - /student/signin : 로그인    


## STUDENT_REGISTER
```javascript
    let body = '';
    req.on('data', function (data) {
        body += data;            
    });
    req.on('end', function () {
        const json =JSON.parse(body);
        const student = new Student(json.name, json.id, json.pwd, json.school);
        //입력하려는 학생 객체를 class를 이용해서 만들었습니다.
        let studentArrays = [];
        let newDb;
        fs.readFile('database.json', 'utf8', function(err, data) {                
            if(data){
                let originDb = JSON.parse(data);
                let originSchoolDb = originDb.school;     
                let originStduentDb = originDb.student;   
                let schoolArrayDb = Array.from(originSchoolDb);
                //기존에 student database가 존재하는 지 확인합니다.          
                if(originStduentDb){
                    studentArrays = Array.from(originStduentDb)
                    //학생 목록을 array로 바꾸어 줍니다.
                    //기존 학교 데이터를 가져 옵니다.
                    if(schoolArrayDb.length > 0){
                        for(let i=0;i<schoolArrayDb.length;i++){
                            let school = schoolArrayDb[i];
                            //이미 데이터에 존재하는 학교인지를 if 문을 통해 체크 합니다.
                            if(school.name == student.school){          
                                studentArrays.push(student);
                                //학생 목록에 신규 학생을 추가합니다.
                                originDb.student = studentArrays
                                newDb = JSON.stringify(originDb);
                                fs.writeFile('database.json', newDb, 'utf8', function(err) {
                                    res.statusCode = 201;
                                    res.end(JSON.stringify({result : true, list : newDb}))
                                });
                                break;
                            } else {
                                res.statusCode = 404;
                                // 404 : data not found
                                res.end(JSON.stringify({result : false}))    
                            }
                        }
                    } else {
                        //학교 데이터가 없기 때문에 학생 데이터를 입력할 수 없습니다.
                        res.statusCode = 404;
                        // 404 : data not found
                        res.end(JSON.stringify({result : false}))                                    
                    }
                    
                } else {
                    if(schoolArrayDb.length > 0){
                        for(let i=0;i<schoolArrayDb.length;i++){
                            let school = schoolArrayDb[i];
                            //이미 데이터에 존재하는 학교인지를 if 문을 통해 체크 합니다.
                            if(school.name == student.school){ 
                                studentArrays.push(student);  
                                originDb.student = studentArrays     
                                newDb = JSON.stringify(originDb);
                                fs.writeFile('database.json', newDb, 'utf8', function(err) {
                                    res.statusCode = 201;
                                    res.end(JSON.stringify({result : true, list : newDb}))
                                });
                                break;
                            } else {
                                res.statusCode = 404;
                                // 404 : data not found
                                res.end(JSON.stringify({result : false}))    
                            }
                        }
                    } else {
                        //학교 데이터가 없기 때문에 학생 데이터를 입력할 수 없습니다.
                        res.statusCode = 404;
                        // 404 : data not found
                        res.end(JSON.stringify({result : false}))                                    
                    }
                    
                }                         
            }
        });            
    });
}      
```


## STUDENT_SIGNIN
```javascript
    if(url.pathname == '/student/signin'){
        let body = '';
        req.on('data', function (data) {
            body += data;            
        });
        req.on('end', function () {
            const jsosn =JSON.parse(body);
            fs.readFile('database.json', 'utf8', function(err, data) {                
                if(data){
                    const json =JSON.parse(body);
                    let originDb = JSON.parse(data);
                    let originStduentDb = originDb.student;               
                    //우선 기존에 학생 데이터가 있는 지를 if 문으로 체크합니다.
                    if(originStduentDb){
                        let arrayDb = Array.from(originStduentDb);
                        for(let i=0;i<arrayDb.length;i++){
                            let student = arrayDb[i];
                            if(jsosn.id == student.id && json.pwd == student.pwd){                                   
                                //데이터가 존재하면 로그인이 가능합니다.
                                //true를 호출합니다.
                                res.end(JSON.stringify({login : true}))
                                break;
                            } else {
                                console.log('no shool with that name ')
                            }
                            res.end(JSON.stringify({login : false}))
                        }
                    } else {
                        //학생 데이터가 없다면 로그인에 실패합니다.
                        res.end(JSON.stringify({login : false}))
                    }                         
                }
            });            
        });
    }
```


## STUDENT_PUT
```javascript
    let body = '';
    req.on('data', function (data) {
        body += data;            
    });
    req.on('end', function () {
        const json =JSON.parse(body);
        const student = new Student(json.name, json.id, json.pwd, json.school);
        fs.readFile('database.json', 'utf8', function(err, data) {                
            let originDb = JSON.parse(data);                    
            let originSchoolDb = originDb.school;     
            let originStduentDb = originDb.student;   
            let schoolArrayDb = Array.from(originSchoolDb);              
            if(originStduentDb){
                studentArrays = Array.from(originStduentDb)
                //기존 학교 데이터를 가져 옵니다.
                if(schoolArrayDb.length > 0){
                    for(let i=0;i<schoolArrayDb.length;i++){
                        let school = schoolArrayDb[i];
                        //이미 데이터에 존재하는 학교인지를 if 문을 통해 체크 합니다.
                        if(school.name == student.school){          
                            //학생 목록을 변경한 array로 바꾸어 줍니다.
                            studentArrays[json.index]  = student;     
                            originDb.student = studentArrays
                            newDb = JSON.stringify(originDb);
                            fs.writeFile('database.json', newDb, 'utf8', function(err) {
                                res.statusCode = 201;
                                res.end(JSON.stringify({result : true, list : newDb}))
                            });
                            break;
                        } else {
                            res.statusCode = 404;
                            // 404 : data not found
                            res.end(JSON.stringify({result : false}))    
                        }
                    }
                } else {
                    //학교 데이터가 없기 때문에 학생 데이터를 입력할 수 없습니다.
                    res.statusCode = 404;
                    // 404 : data not found
                    res.end(JSON.stringify({result : false}))                                    
                }
            }
        });
    });   
```


## STUDENT_DELETE
```javascript
    let body = '';
    req.on('data', function (data) {
        body += data;            
    });
    req.on('end', function () {
        const json =JSON.parse(body);
        fs.readFile('database.json', 'utf8', function(err, data) {                
            let originDb = JSON.parse(data);                    
            let originStduentDb = originDb.student;               
            if(originStduentDb){
                let arrayDb = Array.from(originStduentDb);
                delete arrayDb[json.index] 
                
                arrayDb = arrayDb.filter(x => x != null)               
                originDb.student = arrayDb
                fs.writeFile('database.json', JSON.stringify(originDb), 'utf8', function(err) {
                    res.statusCode = 201;
                    res.end(JSON.stringify({result : true, list : originDb}))
                });
            }
        });
    });

```

## 전체코드
```javascript
const http = require('http');
const fs = require('fs');
const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
    const url = require('url').parse(req.url, true);    
    console.log('url : ',url)
    if(url.pathname == '/school'){
        if(req.method == 'POST'){
            let body = '';
            req.on('data', function (data) {
                body += data;                            
            });
            req.on('end', function () {
                const json =JSON.parse(body);
                const school = new School(json.name, json.address, json.type);
                let schoolArrays = [school];
                let newDb;
                fs.readFile('database.json', 'utf8', function(err, data) {                
                    if(data){
                        let originDb = JSON.parse(data);
                        console.log('originDb : ',originDb)
                        //점 표기법으로 school을 불러옵니다.
                        let originSchoolObj = originDb.school
                        let arrayDb = Array.from(originSchoolObj);
                        arrayDb.push(school);
                        originDb.school = arrayDb;      

                        console.log('originDb : ',originDb)   
                        newDb = JSON.stringify(originDb)                                                             
                    } else {
                        // no data exists                      
                        newDb = JSON.stringify({school: schoolArrays});
                    }

                    fs.writeFile('database.json', newDb, 'utf8', function(err) {
                        res.statusCode = 201;
                        res.end(JSON.stringify({result : true, list : newDb}))
                    });
                });            
            });

        } else if(req.method == 'PUT'){
            let body = '';
            req.on('data', function (data) {
                body += data;            
            });
            req.on('end', function () {
                const json =JSON.parse(body);
                const school = new School(json.name, json.address, json.type);
                fs.readFile('database.json', 'utf8', function(err, data) {                
                    let originDb = JSON.parse(data);   
                    let originSchoolObj = originDb.school     
                    let arrayDb = Array.from(originSchoolObj);
                    arrayDb[json.index]  = school;
                    originDb.school = arrayDb;            
                    let newDb = JSON.stringify(originDb)             
                    fs.writeFile('database.json', newDb, 'utf8', function(err) {
                        res.statusCode = 201;
                        res.end(JSON.stringify({result : true, list : arrayDb}))
                    });
                });
            });   
        } else if(req.method == 'DELETE'){
            let body = '';
            req.on('data', function (data) {
                body += data;            
            });
            req.on('end', function () {
                const json =JSON.parse(body);
                fs.readFile('database.json', 'utf8', function(err, data) {                
                    let originDb = JSON.parse(data);                    
                    let originSchoolObj = originDb.school                 
                    let arrayDb = Array.from(originSchoolObj);
                    delete arrayDb[json.index] 
                    arrayDb = arrayDb.filter(x => x != null)   
                    originDb.school = arrayDb;            
                    let newDb = JSON.stringify(originDb)
                    fs.writeFile('database.json', newDb, 'utf8', function(err) {
                        res.statusCode = 201;
                        res.end(JSON.stringify({result : true, list : arrayDb}))
                    });
                });
            });   
        } else {                    
            res.statusCode = 200;
            // res.end('Hello World\n');
            fs.readFile('database.json', 'utf8', function(err, data) {                
                res.end(JSON.stringify({result : true, list : data}))
            });


        }


    } else if (url.pathname.includes('/student')){

        if(req.method == 'POST'){
            console.log("post called")  
            if(url.pathname == '/student/signin'){
                let body = '';
                req.on('data', function (data) {
                    body += data;            
                });
                req.on('end', function () {
                    const jsosn =JSON.parse(body);
                    fs.readFile('database.json', 'utf8', function(err, data) {                
                        if(data){
                            const json =JSON.parse(body);
                            let originDb = JSON.parse(data);
                            let originStduentDb = originDb.student;               
                            //우선 기존에 학생 데이터가 있는 지를 if 문으로 체크합니다.
                            if(originStduentDb){
                                let arrayDb = Array.from(originStduentDb);
                                for(let i=0;i<arrayDb.length;i++){
                                    let student = arrayDb[i];
                                    if(json.id == student.id && json.pwd == student.pwd){                                   
                                        res.end(JSON.stringify({login : true}))
                                        break;
                                    } else {
                                        console.log('no shool with that name ')
                                    }
                                    res.end(JSON.stringify({login : false}))
                                }
                            } else {
                                //학생 데이터가 없다면 로그인에 실패합니다.
                                //no student exists
                                res.end(JSON.stringify({login : false}))
                            }                         
                        }
                    });            
                });
            } else {
                let body = '';
                req.on('data', function (data) {
                    body += data;            
                });
                req.on('end', function () {
                    const json =JSON.parse(body);
                    const student = new Student(json.name, json.id, json.pwd, json.school);
                    //입력하려는 학생 객체를 class를 이용해서 만들었습니다.
                    let studentArrays = [];
                    let newDb;
                    fs.readFile('database.json', 'utf8', function(err, data) {                
                        if(data){
                            let originDb = JSON.parse(data);
                            let originSchoolDb = originDb.school;     
                            let originStduentDb = originDb.student;   
                            let schoolArrayDb = Array.from(originSchoolDb);
                            //기존에 student database가 존재하는 지 확인합니다.          
                            if(originStduentDb){
                                studentArrays = Array.from(originStduentDb)
                                //기존 학교 데이터를 가져 옵니다.
                                if(schoolArrayDb.length > 0){
                                    for(let i=0;i<schoolArrayDb.length;i++){
                                        let school = schoolArrayDb[i];
                                        //이미 데이터에 존재하는 학교인지를 if 문을 통해 체크 합니다.
                                        if(school.name == student.school){          
                                            studentArrays.push(student);
                                            //학생 목록을 변경한 array로 바꾸어 줍니다.
                                            originDb.student = studentArrays
                                            newDb = JSON.stringify(originDb);
                                            fs.writeFile('database.json', newDb, 'utf8', function(err) {
                                                res.statusCode = 201;
                                                res.end(JSON.stringify({result : true, list : newDb}))
                                            });
                                            break;
                                        } else {
                                            if(i == (schoolArrayDb.length-1)){
                                                res.statusCode = 404;
                                                // 404 : data not found
                                                res.end(JSON.stringify({result : false}))                                            
                                            }
                                        }
                                    }
                                } else {
                                    //학교 데이터가 없기 때문에 학생 데이터를 입력할 수 없습니다.
                                    res.statusCode = 404;
                                    // 404 : data not found
                                    res.end(JSON.stringify({result : false}))                                    
                                }

                            } else {
                                // student no existt
                                if(schoolArrayDb.length > 0){
                                    for(let i=0;i<schoolArrayDb.length;i++){
                                        let school = schoolArrayDb[i];
                                        //이미 데이터에 존재하는 학교인지를 if 문을 통해 체크 합니다.
                                        if(school.name == student.school){ 
                                            studentArrays.push(student);  
                                            originDb.student = studentArrays     
                                            newDb = JSON.stringify(originDb);
                                            fs.writeFile('database.json', newDb, 'utf8', function(err) {
                                                res.statusCode = 201;
                                                res.end(JSON.stringify({result : true, list : newDb}))
                                            });
                                            break;
                                        } else {
                                            if(i == (schoolArrayDb.length-1)){
                                                res.statusCode = 404;
                                                // 404 : data not found
                                                res.end(JSON.stringify({result : false}))                                            
                                            }
                                        } 
                                    }
                                } else {
                                    //학교 데이터가 없기 때문에 학생 데이터를 입력할 수 없습니다.
                                    res.statusCode = 404;
                                    // 404 : data not found
                                    res.end(JSON.stringify({result : false}))                                    
                                }

                            }                         
                        }
                    });            
                });
            }      


        } else if(req.method == 'PUT'){
            let body = '';
            req.on('data', function (data) {
                body += data;            
            });
            req.on('end', function () {
                const json =JSON.parse(body);
                const student = new Student(json.name, json.id, json.pwd, json.school);
                fs.readFile('database.json', 'utf8', function(err, data) {                
                    let originDb = JSON.parse(data);                    
                    let originSchoolDb = originDb.school;     
                    let originStduentDb = originDb.student;   
                    let schoolArrayDb = Array.from(originSchoolDb);              
                    if(originStduentDb){
                        studentArrays = Array.from(originStduentDb)
                        //기존 학교 데이터를 가져 옵니다.
                        if(schoolArrayDb.length > 0){
                            for(let i=0;i<schoolArrayDb.length;i++){
                                let school = schoolArrayDb[i];
                                //이미 데이터에 존재하는 학교인지를 if 문을 통해 체크 합니다.
                                if(school.name == student.school){          
                                    //학생 목록을 변경한 array로 바꾸어 줍니다.
                                    studentArrays[json.index]  = student;     
                                    originDb.student = studentArrays
                                    newDb = JSON.stringify(originDb);
                                    fs.writeFile('database.json', newDb, 'utf8', function(err) {
                                        res.statusCode = 201;
                                        res.end(JSON.stringify({result : true, list : newDb}))
                                    });
                                    break;
                                } else {
                                    if(i == (schoolArrayDb.length-1)){
                                        res.statusCode = 404;
                                        // 404 : data not found
                                        res.end(JSON.stringify({result : false}))                                            
                                    }
                                } 
                            }
                        } else {
                            //학교 데이터가 없기 때문에 학생 데이터를 입력할 수 없습니다.
                            res.statusCode = 404;
                            // 404 : data not found
                            res.end(JSON.stringify({result : false}))                                    
                        }
                    }
                });
            });   
        } else if(req.method == 'DELETE'){
            let body = '';
            req.on('data', function (data) {
                body += data;            
            });
            req.on('end', function () {
                const json =JSON.parse(body);
                fs.readFile('database.json', 'utf8', function(err, data) {                
                    let originDb = JSON.parse(data);                    
                    let originStduentDb = originDb.student;               
                    if(originStduentDb){
                        let arrayDb = Array.from(originStduentDb);
                        delete arrayDb[json.index] 

                        arrayDb = arrayDb.filter(x => x != null)               
                        originDb.student = arrayDb
                        fs.writeFile('database.json', JSON.stringify(originDb), 'utf8', function(err) {
                            res.statusCode = 201;
                            res.end(JSON.stringify({result : true, list : originDb}))
                        });
                    }
                });
            });   
        } else {      
            let user = url.query.user;
            console.log('user : ',user)
            res.statusCode = 200;
            // res.end('Hello World\n');
            fs.readFile('database.json', 'utf8', function(err, data) {                
                let originDb = JSON.parse(data);                    
                let originStduentDb = originDb.student; 
                let originSchoolDb = originDb.school; 
                if(originStduentDb){
                    let stduentArrayDb = Array.from(originStduentDb);
                    console.log('stduentArrayDb : ',stduentArrayDb)
                    let schoolArray = Array.from(originSchoolDb);
                    console.log('schoolArray : ',schoolArray)
                    for(let i =0;i<stduentArrayDb.length;i++){
                        let studentItemName = stduentArrayDb[i].name;
                        console.log('studentItemName :',studentItemName)
                        console.log('user :',user)
                        if(studentItemName== user){
                            let checkShool =  stduentArrayDb[i].school                          
                            for(let j=0;j<schoolArray.length;j++){
                                if(schoolArray[j].name == checkShool){
                                    res.end(JSON.stringify({result : true, student : stduentArrayDb[i], school:schoolArray[j]}))
                                    break;
                                }

                            }                            
                            // break;
                        }
                    }
                    res.statusCode = 404;
                    // 404 : data not found
                    res.end(JSON.stringify({result : false}))   
                }    

            });


        }
    }
    class School {
        constructor(name, address, type) {
            this.name = name;
            this.address = address;
            this.type = type;

        }        
    }  

    class Student {
        constructor(name, id, pwd, school) {
            this.name = name;
            this.id = id;
            this.pwd = pwd;
            this.school = school;
        }        
    }  
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
```
  