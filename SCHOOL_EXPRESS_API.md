
---

TeamEverywhere Nodejs Guide
*Updated at 2019-12-01*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [TeamEverywhere](https://www.team-everywhere.com/)*


- [EXPRESS](#EXPRESS)
- [Routing](#Routing)
- [Express의 middleware 사용](#Express의 middleware 사용)
- [Response Method](#Response Method)
- [Express API 4.x](#Express API 4.x)
- [School_Express_API](#School_Express_API)



<!-- END doctoc generated TOC please keep comment here to allow auto update -->
## EXPRESS
    - 우리는 Node의 기본적인 기능만을 위해 라우팅 작업을 했습니다.
    - 작업을 하다 보니 하나 느낄 수 있는 것이 있습니다.
    - HTTP 프로토콜을 일일히 제어하고, 이에 따른 라우팅을 하는 것은 말도 못하게 일이 많다는 것!
    - 때문에 다양한 라우팅 프레임워크를 사용하고, 우리는 그 중 가장 유명한 프레임워크 중 하나인 express를 사용해 보겠습니다.
    - 프레임워크 : 소프트웨어의 구체적인 부분에 해당하는 설계와 구현이 재사용 가능하게끔 클래스들을 제공하는 것
    - https://expressjs.com/


## Routing
    - 기본적인 라우팅은 아래와 같이 사용 합니다.
```javascript
    var express = require('express')
    var app = express()

    // GET method route
    app.get('/', function (req, res) {
        res.send('GET request to the homepage')
    })

    // POST method route
    app.post('/', function (req, res) {
        res.send('POST request to the homepage')
    })
```
    - 필요에 따라 아래와 같은 기능을 사용할 수 있습니다.
```javascript
    // 특정 경로로 라우팅을 해야 할때,
    app.get('/about', function (req, res) {
      res.send('about')
    })

    // 익스프레스 라우팅은 패턴을 감지 합니다.
    // 아래 코드는 acd 혹은 abcd를 라우팅 할 것입니다.
    app.get('/ab?cd', function (req, res) {
      res.send('ab?cd')
    })
    //아래 코드는 abcd, abxcd, abRANDOMcd, ab123cd 등을 라우팅합니다.
    app.get('/ab+cd', function (req, res) {
      res.send('ab+cd')
    })

    //아래 코드는 d가 포함 돼어 있는 경로를 라우팅 합니다.
    app.get(/d/, function (req, res) {
        res.send('/d/')
    })

    //butterfly등 fly앞에 붙어있는 경로를 합쳐서 라우팅 합니다.
    app.get(/.*fly$/, function (req, res) {
        res.send('/.*fly$/')
    })

    //경로에 파라미터를 추가할 수도 있습니다.
    app.get('/users/:userId', function (req, res) {
        res.send(req.params)
    })
```


## Express의 middleware 사용
    - Express에서는 use라는 함수를 이용해 미들웨어를 사용합니다.
    - 미들웨어란 공통 서비스 및 기능을 애플리케이션에 제공하는 소프트웨어입니다.
        - 운영체제와 어플리케이션 가운데에서 기능을 제공하는 모듈을 의미합니다.
        - 만약 제가 리퀘스트 시마다 토큰 인증을 하는 기능을 추가하고 싶다면, 토큰 인증 미들웨어를 추가할 수 있습니다.
        - 그 후에는 이제 모든 리퀘스트 시마다 토큰 인증을 하게 만들 수도 있습니다.
```javascript
    app.use('/', indexRouter);
    app.use('/users', usersRouter);
    app.use('/abcd', abcdRouter);
    app.use('/abc', abcRouter);
```


## Response Method
    - res.download() : 파일 등 프론트엔드에서 다운로드할 수 있게 응답하는 메소드
    - res.end()	: 응답을 종료
    - res.json() : JSON으로 응답
    - res.redirect() : 다른 경로로 변경하는 응답
    - res.render() : ejs 등 템플릿 파일 등을 화면에 그려주는 응답
    - res.send() : 기본적인 응답
    - res.sendFile() : html 등 기본적인 웹 화면을 화면에 그려주는 응답
    - res.sendStatus() : status 코드 응답


## Express API 4.x
```javascript
    app.set('views', path.join(__dirname, 'views'));
    //view의 경로 설정
    app.set('view engine', 'ejs');
    //ejs 템플릿을 사용
    app.use(logger('dev'));
    //logger 모듈을 사용한다면 설정
    //logger 모듈 보다 위에 선언한 모듈에 대해서는 로깅을 받지 않는다.
    //dev 설정을 하면 response에 따라 색이 다른 로그를 보여준다.
    app.use(express.json());
    //헤더의 content type을 자동으로 json으로 설정해 줌
    app.use(express.urlencoded({ extended: false }));
    //한글 등 url을 utf8로 인코딩 할 필요가 있을때 사용
    //보다 다양한 모듈과 형식을 지원하고 싶으면 extended를 true로 설정한다.
    app.use(cookieParser());
    //서버에서 쿠키를 쉽게 생성할 수 있게 해주는 모듈
    //http 프로토콜은 통신이 끝나면 상태 정보를 저장하지 않기 때문에, 유저가 다시 접속 시 이전 화면을 보여주는 등 상태에 대한 저장이 필요할 때 사용
    app.use(express.static(path.join(__dirname, 'public')));
    //static(전 경로에서 참조할 수 있는) 루트 디렉토리를 설정해 줌
```


## School_Express_API
```javascript
var express = require('express');
var router = express.Router();
var fs = require('fs');

class School {
    constructor(name, address, type) {
        this.name = name;
        this.address = address;
        this.type = type;
    }        
}  

router.get('/', function(req, res, next) {
    fs.readFile('database.json', 'utf8', function(err, data) {                
        res.status = 200;
        res.json({result : true, list : data})
    });
});

router.post('/', function (req, res) {
    let json = req.body;
    const school = new School(json.name, json.address, json.type);
    let schoolArrays = [school];
    let newDb;
    fs.readFile('database.json', 'utf8', function(err, data) {                
        if(data){
            let originDb = JSON.parse(data);
            let originSchoolObj = originDb.school
            let arrayDb = Array.from(originSchoolObj);
            arrayDb.push(school);
            originDb.school = arrayDb;            
            newDb = JSON.stringify(originDb)                                                             
        } else {
            // no data exists
            newDb = JSON.stringify({school: schoolArrays});
        }        
        fs.writeFile('database.json', newDb, 'utf8', function(err) {
            res.statusCode = 201;
            res.end(JSON.stringify({result : true, list : newDb}))
        });
    });          
})

router.put('/', function (req, res) {
    let json = req.body;
    const school = new School(json.name, json.address, json.type);
    fs.readFile('database.json', 'utf8', function(err, data) {                
        let originDb = JSON.parse(data);   
        let originSchoolObj = originDb.school                 
        let arrayDb = Array.from(originSchoolObj);
        arrayDb[json.index]  = school;
        originDb.school = arrayDb;            
        let newDb = JSON.stringify(originDb)             
        fs.writeFile('database.json', newDb, 'utf8', function(err) {
            res.statusCode = 201;
            res.end(JSON.stringify({result : true, list : arrayDb}))
        });
    });  
})

router.delete('/', function (req, res) {
    let json = req.body;
    fs.readFile('database.json', 'utf8', function(err, data) {                
        let originDb = JSON.parse(data);                    
        let originSchoolObj = originDb.school                 
        let arrayDb = Array.from(originSchoolObj);
        delete arrayDb[json.index] 
        arrayDb = arrayDb.filter(x => x != null)   
        originDb.school = arrayDb;            
        let newDb = JSON.stringify(originDb)
        fs.writeFile('database.json', newDb, 'utf8', function(err) {
            res.statusCode = 201;
            res.end(JSON.stringify({result : true, list : arrayDb}))
        });
    });
})


module.exports = router;
//주어진 소스를 모듈로 리턴하는 코드
```
